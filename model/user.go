package model

import (
	"context"

	"bee2bee/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
	Password    string `json:"password"`
}

func UserOne(filter bson.M) interface{} {
	var user User
	collection := db.GetCollection()
	result := collection.FindOne(context.TODO(), filter, options.FindOne().SetProjection(bson.M{"password": 0}))
	result.Decode(&user)
	return user
}

func UserInsert(user *User) interface{} {
	collection := db.GetCollection()
	insertResult, _ := collection.InsertOne(context.TODO(), user)
	return insertResult.InsertedID
}
