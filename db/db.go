package db

import (
	"context"
	"log"

	// "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

var _client *mongo.Client
var _collection *mongo.Collection
var err error

//Init initialize mongodb client connection
func Init() {
	clientOptions := options.Client().ApplyURI("mongodb://192.168.56.101:27017")
	_client, err = mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = _client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	_collection = _client.Database("bee2bee").Collection("user")
}

//GetClient retrieve current mongodb client
func GetClient() *mongo.Client {
	return _client
}

func GetCollection() *mongo.Collection {
	return _collection
}
