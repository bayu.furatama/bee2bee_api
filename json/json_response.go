package json

type ResponseStruct struct {
	Ok         bool        `json:"ok"`
	StatusCode string      `json:"statusCode"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

//ResponseOK return an ok response
func ResponseOK(intr interface{}, statusCode string, message string) interface{} {
	response := ResponseStruct{
		Ok:         true,
		StatusCode: statusCode,
		Message:    message,
		Data:       intr,
	}

	return response
}
