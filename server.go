package main

import (
	"bee2bee/db"
	"bee2bee/route"
	// "context"
	// "log"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

func main() {
	// Routes
	db.Init()
	route.Init()

}

// func main() {
// 	c := db.GetClient()
// 	err := c.Ping(context.Background(), readpref.Primary())
// 	if err != nil {
// 		log.Fatal("Couldn't connect to the database", err)
// 	} else {
// 		log.Println("Connected!")
// 	}
// }
