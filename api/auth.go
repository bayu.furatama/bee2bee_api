package api

import (
	"bee2bee/json"
	"bee2bee/model"
	"encoding/base64"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
)

//AuthUserLogin is done via login
func AuthUserLogin(c echo.Context) (err error) {
	authRequest := c.Request().Header.Get("Authorization")
	authRequest = strings.ReplaceAll(authRequest, "Basic ", "")
	authData, _ := base64.StdEncoding.DecodeString(authRequest)
	authDataDecoded := string(authData)
	authDataString := strings.Split(authDataDecoded, ":")

	email := authDataString[0]
	password := authDataString[1]

	res := model.UserOne(bson.M{"email": email, "password": password})

	return c.JSON(http.StatusOK, json.ResponseOK(res, "auth_success", "Login success"))
}
