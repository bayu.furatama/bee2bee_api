package api

import (
	"bee2bee/json"
	"bee2bee/model"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
)

//UserRegistration is done by registration user
func UserRegistration(c echo.Context) (err error) {
	u := new(model.User)
	c.Bind(u)
	newID := model.UserInsert(u)
	res := model.UserOne(bson.M{"_id": newID})

	return c.JSON(http.StatusOK, json.ResponseOK(res, "user_registration_success", "User registered successfully"))
}
